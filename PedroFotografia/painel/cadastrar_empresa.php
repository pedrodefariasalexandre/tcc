﻿

<?php
	include "conexao.php";
	include('validar.php');
?>
<!DOCTYPE html>
<html lang="pt-br">
	<head>
		<title></title>
		<style type="text/css">
			.erro {
				color: red;
			}
		</style>
	</head>
	<body>
		<?php
			$erro = @$_GET['erro'];
			if ($erro) {
				echo "<span class=\"erro\">Não foi possível cadastrar a Empresa! <br>Mensagem: $erro</span>";
			}
		?>
		<form action="cadastrar_empresa_db.php" method="post" enctype="multipart/form-data">			
			<label for="descricao">Descrição: </label><br>
			<input type="text" name="descricao" id="descricao" maxlength="500" placeholder="Z O C A   F O T O G R A F I A"><br><br>
			
			<label for="missao">Missão: </label><br>
			<input type="text" name="missao" id="missao" maxlength="200" placeholder="Missão: Superar as expectativas e atender as necessidades e ideias de cada cliente, criando assim fortes e duradouros vínculos com nossos consumidores."><br><br>

			<label for="visao">Visão</label><br>
			<input type="text" name="visao" id="visao" maxlength="200" placeholder="Visão: Ser uma empresa de fotografia referência na área, com excelência em atendimento ao cliente e resultados de alta qualidade."><br><br>
			
			<label for="visao">Valores:</label><br>
			<input type="text" name="valores" id="valores" maxlength="200" placeholder="Valores: Atuar com profissionalismo e dedicação, oferecer o diferencial, proporcionando atendimento exclusivo e personalizado, trabalhar pelo melhor resultado e valorizar a confiança de nossos clientes."><br><br>
			
			
			<input type="submit" name="cadastrar "value="Cadastrar">
		</form>
	</body>
</html>