﻿

<?php
	include "conexao.php";
	include('validar.php');
?>
<?php
	include("conexao.php");
?>
<!DOCTYPE html>
<html lang="pt-br">
	<head>
		<title></title>
		<style type="text/css">
			.sucesso {
				color: green;
			}
			.erro {
				color: red;
			}
		</style>
	</head>
	<body>
		<?php
			include("menu.php");
			$erro = @$_GET['erro'];
			if (@$_GET['ok'] == 1) {
				echo "<span class=\"sucesso\">Empresa cadastrada com sucesso!</span><br>";
			} elseif (@$_GET['ok'] == 2) {
				echo "<span class=\"sucesso\">Empresa alterada com sucesso!</span><br>";
			} elseif (@$_GET['ok'] == 3) {
				echo "<span class=\"sucesso\">Empresa excluída com sucesso!</span><br>";
			} elseif ($erro) {
				echo "<span class=\"erro\">Não foi possível excluir a Empresa!<br> Mensagem: $erro</span><br>";
			}
		?>
		<a href="cadastrar_empresa.php">Cadastrar</a>
		<table>
			<tr>
				<th>Código|</th>
				<th>Descrição|</th>
				<th>Missão|</th>
				<th>Visão|</th>
				<th>Valores</th>
				<th></th>
			</tr>
		<?php
			$sql = "SELECT * FROM empresa";
			$retorno = mysqli_query($mysql, $sql);
			if(!$retorno) {
				echo mysqli_error($mysql);
			}
			while($obj = mysqli_fetch_array($retorno, MYSQLI_ASSOC)) {
		?>
			<tr>
				<td><?php echo $obj['id']; ?></td>
				<td><?php echo $obj['descricao']; ?></td>
				<td><?php echo $obj['missao']; ?></td>
				<td><?php echo $obj['visao']; ?></td>
				<td><?php echo $obj['valores']; ?></td>
				<td>
					<a href="alterar_empresa.php?id=<?php echo $obj['id']; ?>">
						<img src="ico-editar.png">
					</a>
				</td>
			</tr>
		<?php
			}
		?>
		</table>
		Existe(m)
		<?php
			echo mysqli_num_rows($retorno);
		?>
		Registro(s)
	</body>
</html>
<?php
	mysqli_close($mysql);
?>