﻿

<?php
	include("conexao.php");
	include('validar.php');
?>
<!DOCTYPE html>
<html lang="pt-br">
	<head>
		<title></title>
		<style type="text/css">
			.sucesso {
				color: green;
			}
			.erro {
				color: red;
			}
		</style>
	</head>
	<body>
		<?php
			include("menu.php");
			$erro = @$_GET['erro'];
			if (@$_GET['ok'] == 1) {
				echo "<span class=\"sucesso\">Usuário cadastrado com sucesso!</span><br>";
			} elseif (@$_GET['ok'] == 2) {
				echo "<span class=\"sucesso\">Usuário alterado com sucesso!</span><br>";
			} elseif (@$_GET['ok'] == 3) {
				echo "<span class=\"sucesso\">Usuário excluído com sucesso!</span><br>";
			} elseif ($erro) {
				echo "<span class=\"erro\">Não foi possível excluir o usuário!<br> Mensagem: $erro</span><br>";
			}
		?>
		<a href="cadastrar_usuarios.php">Cadastrar</a>
		<table>
			<tr>
				<th>Código|</th>
				<th>Login|</th>
				<th>Status</th>
				<th></th>
				<th></th>
			</tr>
		<?php
			$sql = "SELECT * FROM usuarios";
			$retorno = mysqli_query($mysql, $sql);
			if(!$retorno) {
				echo mysqli_error($mysql);
			}
			while($obj = mysqli_fetch_array($retorno, MYSQLI_ASSOC)) {
		?>
			<tr>
				<td><?php echo $obj['id']; ?></td>
				<td><?php echo $obj['login']; ?></td>
				<td><?php echo $obj['status']; ?></td>
				<td>
					<a href="alterar_usuarios.php?id=<?php echo $obj['id']; ?>">
						<img src="ico-editar.png">
					</a>
				</td>
				<td>
					<a href="excluir_usuarios_db.php?id=<?php echo $obj['id']; ?>">
						<img src="ico-excluir.png">
					</a>
				</td>
			</tr>
		<?php
			}
		?>
		</table>
		Existe(m)
		<?php
			echo mysqli_num_rows($retorno);
		?>
		Registro(s)
	</body>
</html>
<?php
	mysqli_close($mysql);
?>