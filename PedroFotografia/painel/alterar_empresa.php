﻿

<?php
	include "conexao.php";
	include('validar.php');
?>
<?php
	include("conexao.php");
?>
<!DOCTYPE html>
<html lang="pt-br">
	<head>
		<title></title>
		<style type="text/css">
			.erro {
				color: red;
			}
		</style>
	</head>
	<body>
		<?php
			$erro = @$_GET['erro'];
			if ($erro) {
				echo "<span class=\"erro\">Não foi possível alterar a empresa!<br> Mensagem: $erro</span>";
			}
			
			$id = $_GET['id'];
			$sql = "SELECT * FROM empresa WHERE id = $id";
			$retorno = mysqli_query($mysql, $sql);
			$obj = mysqli_fetch_array($retorno, MYSQLI_ASSOC);
		?>
		<form action="alterar_empresa_db.php" method="post">
			<input type="hidden" name="id" value="<?php echo $id; ?>">
			
			<label for="descricao">Descrição:</label><br>
			<input type="text" name="descricao" id="descricao" value="<?php echo $obj['descricao']; ?>"><br><br>
			
			<label for="missao">Missão:</label><br>
			<input type="text" name="missao" id="missao" maxlength="200" value="<?php echo $obj['missao']; ?>"><br><br>
			
			<label for="visao">Visão:</label><br>
			<input type="text" name="visao" id="visao" maxlength="200" value="<?php echo $obj['visao']; ?>"><br><br>
			
			<label for="valores">Valores:</label><br>
			<input type="text" name="valores" id="valores" maxlength="200" value="<?php echo $obj['valores']; ?>"><br><br>
						
			<input type="submit" value="Alterar">
		</form>
	</body>
</html>
<?php
	mysqli_close($mysql);
?>