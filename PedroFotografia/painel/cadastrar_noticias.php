﻿

<?php
	include "conexao.php";
	include('validar.php');
?>
<!DOCTYPE html>
<html lang="pt-br">
	<head>
		<title></title>
		<style type="text/css">
			.erro {
				color: red;
			}
		</style>
	</head>
	<body>
		<?php
			$erro = @$_GET['erro'];
			if ($erro) {
				echo "<span class=\"erro\">Não foi possível cadastrar a Notícia! <br>Mensagem: $erro</span>";
			}
		?>
		<form action="cadastrar_noticias_db.php" method="post" enctype="multipart/form-data">
			<label for="titulo">Titulo:</label><br>
			<input type="text" name="titulo" id="titulo" maxlength="100"><br><br>
			
			<label for="descricao">Descrição:</label><br>
			<input type="text" name="descricao" id="descricao" maxlength="200"><br><br>
			
			<label for="arquivo">Imagem:</label><br>
			<input type="file" id="arquivo" name="arquivo"/><br><br>
			
			<input type="submit" name="cadastrar "value="Cadastrar">
		</form>
	</body>
</html>