﻿

<?php
	include "conexao.php";
	include('validar.php');
?>
<?php
	include("conexao.php");
?>
<!DOCTYPE html>
<html lang="pt-br">
	<head>
		<title></title>
		<style type="text/css">
			.sucesso {
				color: green;
			}
			.erro {
				color: red;
			}
		</style>
	</head>
	<body>
		<?php
			include("menu.php");
			$erro = @$_GET['erro'];
			if (@$_GET['ok'] == 1) {
				echo "<span class=\"sucesso\">Portfólio cadastrado com sucesso!</span><br>";
			} elseif (@$_GET['ok'] == 2) {
				echo "<span class=\"sucesso\">Portfólio alterado com sucesso!</span><br>";
			} elseif (@$_GET['ok'] == 3) {
				echo "<span class=\"sucesso\">Portfólio excluído com sucesso!</span><br>";
			} elseif ($erro) {
				echo "<span class=\"erro\">Não foi possível excluir o Portfólio!<br> Mensagem: $erro</span><br>";
			}
		?>
		<a href="cadastrar_portfolios.php">Cadastrar</a>
		<table>
			<tr>
				<th>Código|</th>
				<th>Descrição|</th>
				<th>Imagem|</th>
				<th>Nome da Imagem|</th>
				<th>Caminho</th>
				<th></th>
				<th></th>
			</tr>
		<?php
			$sql = "SELECT * FROM portfolios";
			$retorno = mysqli_query($mysql, $sql);
			if(!$retorno) {
				echo mysqli_error($mysql);
			}
			while($obj = mysqli_fetch_array($retorno, MYSQLI_ASSOC)) {
		?>
			<tr>
				<td><?php echo $obj['id']; ?></td>
				<td><?php echo $obj['descricao']; ?></td>
				<td><img width="200px" height="150px" src="Portfolios/<?php echo $obj['arquivo_nome']?>"/></td>
				<td><?php echo $obj['arquivo_nome']; ?></td>
				<td><?php echo $obj['arquivo_local']; ?></td>
				<td>
					<a href="alterar_portfolios.php?id=<?php echo $obj['id']; ?>">
						<img src="ico-editar.png">
					</a>
					
					<!-- <form action="alterar_clientes.php" method="post">
						<input type="hidden" name="id" value="<?php echo $obj['id']; ?>">
						<input type="submit" value="Editar">
					</form> -->
				</td>
				<td>
					<a href="excluir_portfolios_db.php?id=<?php echo $obj['id']; ?>">
						<img src="ico-excluir.png">
					</a>
				</td>
			</tr>
		<?php
			}
		?>
		</table>
		<table>
		Existe(m)
		<?php
			echo mysqli_num_rows($retorno);
		?>
		Registro(s)
		</table>
	</body>
</html>
<?php
	mysqli_close($mysql);
?>