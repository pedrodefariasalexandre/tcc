﻿

<?php
	include("conexao.php");
	include('validar.php');
?>
<!DOCTYPE html>
<html lang="pt-br">
	<head>
		<title></title>
		<style type="text/css">
			.erro {
				color: red;
			}
		</style>
	</head>
	<body>
		<?php
			$erro = @$_GET['erro'];
			if ($erro) {
				echo "<span class=\"erro\">Não foi possível alterar o usuário!<br> Mensagem: $erro</span>";
			}
			
			$id = $_GET['id'];
			$sql = "SELECT * FROM usuarios WHERE id = $id";
			$retorno = mysqli_query($mysql, $sql);
			$obj = mysqli_fetch_array($retorno, MYSQLI_ASSOC);
		?>
		<form action="alterar_usuarios_db.php" method="post">
			<input type="hidden" name="id" value="<?php echo $id; ?>">
			
			<label for="login">Login:</label><br>
			<input type="text" name="login" id="login" maxlength="20" value="<?php echo $obj['login']; ?>"><br><br>
			
			<label for="senha">Senha:</label><br>
			<input type="password" name="senha" id="senha" maxlength="30" value=""><br><br>
			
			<label for="status">Status:</label><br>
			<input type="checkbox" name="status" id="status" <?php if ($obj['status'] == 'S') { ?>checked="checked"<?php } ?>><br><br>
						
			<input type="submit" value="Alterar">
		</form>
	</body>
</html>
<?php
	mysqli_close($mysql);
?>