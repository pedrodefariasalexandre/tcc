
<?php
	include("painel/conexao.php");
?>
<!DOCTYPE HTML>
<html lang="pt-br">
<head>
	<title>F O T O G R A F I A</title>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<link rel="stylesheet" type="text/css" href="css/style.css"/>
	<link rel="stylesheet" type="text/css" href="css/menu.css"/>
</head>
<body>
	<div id="topo">
		<div id="imagemTopo">
			<img height="140px" src="img/logoTopo.jpg"/>
		</div>
	</div>
	<div id="barra">
	</div>
	<div id="pagina">
		<div id="conteudo">
			<div id="menu">
				<p class="conteudo_menu">
					<div class="corpo_menu">
						<ul>
							<li class="dropdown">
								<input type="checkbox" />
								<a href="#" data-toggle="dropdown">Pagina Inicial</a>
									<ul class="dropdown-menu">
										<li><a href="index.php">Home</a></li>
										<li><a href="empresa.php">Empresa</a></li>
										<li><a href="clientes.php">Clientes</a></li>
										<li><a href="portfolios.php">Portfólios</a></li>
										<li><a href="noticias.php">Notícias</a></li>
										<li><a href="contato.php">Contato</a></li>
									</ul>
							</li>
							<li class="dropdown">
								<input type="checkbox" />
								<a href="#" data-toggle="dropdown">Painel</a>
									<ul class="dropdown-menu">
										<li><a href="painel/index.php">Login</a></li>
									</ul>
							</li>
						</ul>
					</div>
				</p>
			</div>
			<div id="divisoria">
				<hr size="600px" width="1" align="left" color="#E8E8E8">
			</div>
			<br>
			<br>
			<h4 class="titulo">Notícias</h4>
			<div style="overflow:auto" id="noticias">
				<br><br><br>
				<table width="95%">
					<?php
					//Criar SQL de consulta
					$sql = "SELECT * FROM noticias";
					$retorno = mysqli_query($mysql, $sql);
					if(!$retorno) {
						
						echo mysqli_error($mysql);
					}
					while($obj = mysqli_fetch_array($retorno, MYSQLI_ASSOC)) {
					?>
					<tr>
						<td>
							<b class="titulo"><?php echo $obj['titulo']; ?></b>
							<br><br>
						</td>
					</tr>
					<tr>
						<td width="30%" height="150px"><img src="painel/noticias/<?php echo $obj['arquivo_nome']?>" width="95%" height="100%"></td>
						<td width="70%">
							<p><?php echo $obj['descricao']; ?></p>
						</td>
					</tr>
					<tr>
						<td><br><br></td>
					</tr>
					<?php
					}
					?>
				</table>
			</div>
		</div>
	</div>
<div id="rodape" class="rodape_texto">Todos os direitos reservados a Pedro
</div>
</body>
</html>