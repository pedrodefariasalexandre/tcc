
<!DOCTYPE HTML>
<html lang="pt-br">
<head>
	<title>F O T O G R A F I A</title>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<link rel="stylesheet" type="text/css" href="css/style.css"/>
	<link rel="stylesheet" type="text/css" href="css/menu.css"/>
	<script type="text/javascript">
		function verificacao() {
				var cpNome = document.getElementById('Cnome');
				var cpEmail = document.getElementById('Cemail');
				var cpAssunto = document.getElementById('Cassunto');
				var cpMensagem = document.getElementById('Cmensagem');
				
		if (cpNome.value == "") {
					alert('Campo NOME é obrigatório, preencha o campo!');
					cpNome.focus();
					return false;
				}

				if (cpEmail.value == "") {
					alert('Campo E-MAIL é obrigatório, preencha o campo!');
					cpEmail.focus();
					return false;
				}

				if (cpAssunto.value == "") {
					alert('Campo ASSUNTO é obrigatório, preencha o campo!');
					cpAssunto.focus();
					return false;
				}

				if (cpMensagem.value == "") {
					alert('Campo MENSAGEM é obrigatório, preencha o campo!');
					cpMensagem.focus();
					return false;
				}
				return true;
			}
	</script>
</head>
<body>
	<div id="topo">
		<div id="imagemTopo">
			<img height="140px" src="img/logoTopo.jpg"/>
		</div>
	</div>
	<div id="barra">
	</div>
	<div id="pagina">
		<div id="conteudo">
			<div id="menu">
				<p class="conteudo_menu">
					<div class="corpo_menu">
						<ul>
							<li class="dropdown">
								<input type="checkbox" />
								<a href="#" data-toggle="dropdown">Pagina Inicial</a>
									<ul class="dropdown-menu">
										<li><a href="index.php">Home</a></li>
										<li><a href="empresa.php">Empresa</a></li>
										<li><a href="clientes.php">Clientes</a></li>
										<li><a href="portfolios.php">Portfólios</a></li>
										<li><a href="noticias.php">Notícias</a></li>
										<li><a href="contato.php">Contato</a></li>
									</ul>
							</li>
							<li class="dropdown">
								<input type="checkbox" />
								<a href="#" data-toggle="dropdown">Painel</a>
									<ul class="dropdown-menu">
										<li><a href="painel/index.php">Login</a></li>
									</ul>
							</li>
						</ul>
					</div>
				</p>
			</div>
			<div id="divisoria">
				<hr size="600px" width="1" align="left" color="#E8E8E8">
			</div>
			<div id="noticias">
				<h4 class="titulo">Entre em contato conosco</h4>
				<br>
				<?php
				$erro = @$_GET['erro'];
				if (@$_GET['ok'] == 1) {
					echo "<span class=\"sucesso\">Contato cadastrado com sucesso!</span><br>";
				} elseif (@$_GET['ok'] == 2) {
					echo "<span class=\"sucesso\">Contato alterado com sucesso!</span><br>";
				} elseif (@$_GET['ok'] == 3) {
					echo "<span class=\"sucesso\">Contato excluído com sucesso!</span><br>";
				} elseif ($erro) {
					echo "<span class=\"erro\">Não foi possível enviar o contato!<br> Mensagem: $erro</span><br>";
				}
				?>
				<table>
					<tr>
						<td>
						<br>
						<form action="painel/cadastrar_contato_db.php" method="post" id="formContato" onsubmit="return verificacao();" >
						<table>
							<tr>
								<td>
									Nome:
								</td>
								<td>
									<input type="text" value="" name="nome" id="Cnome" maxlength="50" placeholder="Digite seu Nome">
								</td>
							</tr>
							<tr>
								<td>
									Sexo:
								</td>
								<td>
									<input type="radio" name="sexo" id="feminino" value="F" checked="checked"> Feminino<br>
									<input type="radio" name="sexo" id="masculino" value="M" > Masculino
								</td>
							</tr>
							<tr>
								<td>
									Email:
								</td>
								<td>
									<input type="text" value="" name="email" id="Cemail" maxlength="50" placeholder="Digite seu Email">
								</td>
							</tr>
							<tr>
								<td>
									Telefone:
								</td>
								<td>
									<input type="text" value="" name="telefone" id="Ctelefone" maxlength="13" placeholder="Digite seu Telefone">
								</td>
							</tr>
							<tr>
								<td>
									Empresa:
								</td>
								<td>
									<input type="text" value="" name="empresa" id="Cempresa" maxlength="15" placeholder="Digite a sua Empresa">
								</td>
							</tr>
							<tr>
								<td>
									Assunto:
								</td>
								<td>
									<input type="text" value="" name="assunto" id="Cassunto" maxlength="50" placeholder="Digite seu Assunto">
								</td>
							</tr>
							<tr>
								<td>
									Forma de Contato:
								</td>
								<td>
									<select name="formaContato">
										<option value="formaEmail" id="email">Email</option>
										<option value="formaTelefone" id="telefone">Telefone</option>
									</select>
								</td>
							</tr>
							<tr>
								<td>
									Horário para Contato:
								</td>
								<td>
									<input type="text" value="" name="horario" id="horario" maxlength="10" placeholder="00h00">
								</td>
							</tr>
							<tr>
								<td>
									Mensagem:
								</td>
								<td>
									<textarea rows="7" cols="45" name="mensagem" id="Cmensagem" maxlength="255" placeholder="Digite sua Mensagem"></textarea>
								</td>
							</tr>
							<tr>
								<td>
								</td>
								<td>
									<input type="submit" value="Enviar">
									<input type="reset" value="Limpar">
								</td>
							</tr>
						</table>
						</form>
						</td>
					</tr>
				</table>
			</div>
		</div>
	</div>
<div id="rodape" class="rodape_texto">Todos os direitos reservados a Pedro
</div>
</body>
</html>